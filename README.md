# Aide et ressources de SILX pour Synchrotron SOLEIL

[<img src="https://software.pan-data.eu/cache/8/0/7/9/7/80797e89208390279ed5fa1e8bc791b347084568.png" width="100"/>](https://www.silx.org)

## Résumé

- Exploration & visualisation des données dans les structures de fichier *.nxs ; export des images au format *.edf ; ajsutement de données par des solutions analytiques
- Open source

## Sources

- Code source: https://github.com/silx-kit/silx
- Documentation officielle: http://www.silx.org/doc/silx/latest/index.html

## Navigation rapide

| Tutoriaux | Page pan-data |
| - | - |
| [Tutoriel d'installation officiel](http://www.silx.org/doc/silx/latest/install.html) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/108/silx) |

## Installation

- Systèmes d'exploitation supportés:
- Installation: Déployé dans le package géré par GRADES

## Format de données

- en entrée: fichier *.nxs généré par SOLEIL, sinon fichiers *.txt ou *.dat, images,...
- en sortie: images *.edf pour pyFAI sinon fichier de tableau de données *.txt ou *.dat
- sur la Ruche
